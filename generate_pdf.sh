rm out.pdf
# -v date=$'date +"%d %b %Y"'

pandoc  -f markdown --standalone  --toc --toc-depth=2 --template page.tex --highlight-style pygments -o out.pdf --pdf-engine=xelatex  metadata.yml chapters/*.md