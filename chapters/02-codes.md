\newpage
# Different Codes #


## Quick Sort Algorithm
Like Merge Sort, QuickSort is a Divide and Conquer algorithm. It picks an element as pivot and partitions the given array around the picked pivot. There are many different versions of quickSort that pick pivot in different ways.
\footnote{ This is footnote}


1.  Always pick first element as pivot.
2.  Always pick last element as pivot (implemented below)
3.  Pick a random element as pivot.
4.  Pick median as pivot.

### Python 
    
```python
# Slice off 'node bisect-test.js' from the command line args
def partition(alist,first,last):
   pivotvalue = alist[first]

   leftmark = first+1
   rightmark = last

def quickSort(alist):
   quickSortHelper(alist,0,len(alist)-1)

```

### JS

```javascript
// Slice off 'node bisect-test.js' from the command line args
function swap(items, leftIndex, rightIndex){
    var temp = items[leftIndex];
    items[leftIndex] = items[rightIndex];
    items[rightIndex] = temp;
}
```

### Typescript

```typescript
/**
 * You should have ts-node installed globally before executing this, probably!
 * Otherwise you'll need to compile this script before you start bisecting!
 */
import cp = require('child_process');
import fs = require('fs');

// Slice off 'node bisect-test.js' from the command line args
var args = process.argv.slice(2);

function tsc(tscArgs: string, onExit: (exitCode: number) => void) {
    var tsc = cp.exec('node built/local/tsc.js ' + tscArgs,() => void 0);
    tsc.on('close', tscExitCode => {
        onExit(tscExitCode);
    });
}
```


This is your first markdown chapter!


Name | Lunch order | Spicy      | Owes
------- | ---------------- | ---------- | ---------:
Joan  | saag paneer | medium | $11
Sally  | vindaloo        | mild       | $14
Erin   | lamb madras | HOT      | $5


Include Image 

<div class="center">
![](img/graph2.png)
</div>


See:

* [Monads for Free!](http://www.andres-loeh.de/Free.pdf)
* [I/O is not a Monad](http://r6.ca/blog/20110520T220201Z.html)


### Math 
$\cos (2\theta) = \cos^2 \theta - \sin^2 \theta$
